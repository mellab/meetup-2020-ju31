[![pipeline status](https://gitlab.com/Mellab/meetup-2020-ju31/badges/master/pipeline.svg)](https://gitlab.com/Mellab/meetup-2020-ju31/-/commits/master)

# GitLab Virtual Meetup, 2020-07-31

La presentación por [Alejandro Medina](https://gitlab.com/Mellab): [GitLab CI/CD pipelines + Go](https://docs.google.com/presentation/d/1zgoWWteHAnC-LHGLJ-HRJE2_B-r7N3Bnx8kwwFRp6Oo/edit?usp=sharing) incluye ejercicios de auto-práctica ¡Empieza a aprender de GitLab CI/CD y haz un tweet a [@MentalAbduction](https://twitter.com/MentalAbduction) con tus experiencias y capturas de pantallas!

La configuración CI de solución está incluida en el repositorio en caso de que necesites ayuda con los ejercicios.

Si tienes alguna pregunta para nuestro equipo de la comunidad, puedes contactar a John vía Twitter [@john_cogs](https://twitter.com/john_cogs) o [email](https://mailto:evangelist@gitlab.com).

¡Esperamos verte en el próximo encuentro de GitLab!

- [Event](https://www.meetup.com/es/gitlab-virtual-meetups/events/272087380/)
- [Slides](https://docs.google.com/presentation/d/1zgoWWteHAnC-LHGLJ-HRJE2_B-r7N3Bnx8kwwFRp6Oo/edit?usp=sharing)
